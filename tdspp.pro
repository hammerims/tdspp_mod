QMAKE_CXXFLAGS = -Wall -ansi -fPIC -fpermissive 
QMAKE_LFLAGS += -pie


QT += core
TEMPLATE = lib
CONFIG += shared
VERSION = 1.2.0
TARGET = tds++
LIBS    = -L/usr/lib/x86_64-linux-gnu -lct -shared -ldl
INCLUDEPATH += /usr/include
SOURCES = field.cc \
          query.cc \
	  tdspp.cc 
HEADERS = query.hh \
          tdspp.hh \
          field.hh

#ibtds++.so.1.2 

